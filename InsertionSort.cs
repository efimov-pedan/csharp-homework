﻿using System;

namespace Lesson11
{
	/// <summary>
	/// Реализация сортировки вставками.
	/// </summary>
    class InsertionSort
    {
        public static void Main()
        {
            int[] numbers = { 3, 0, 2, 5, -1, 4, 1 };
            SortArray(numbers);
            PrintArray(numbers);
        }

        private static void SortArray(int[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = i + 1; j > 0; j--)
                {
                    if (array[j - 1] > array[j])
                    {
                        int temp = array[j - 1];
                        array[j - 1] = array[j];
                        array[j] = temp;
                    }
                }
            }
        }

        private static void PrintArray(int[] array)
        {
            foreach (int item in array)
            {
                Console.Write("{0},", item);
            }
        }
    }
}
