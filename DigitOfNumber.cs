﻿using System;

namespace Lesson4
{
	/// <summary>
	/// Поиск сумма чисел числа, сумма чисел только положительных и максимальная цифра числа. 
	/// </summary>
    class NumbersOfNumber
    {
        static void Main()
        {
            Console.WriteLine("введите целое число");
            if (!int.TryParse(Console.ReadLine(), out int userNumber))
            {
                Console.WriteLine("это не число");
                return;
            }

            int remainOfNumber = userNumber;
            int totalSum = 0;
            int sumForOddNumbers = 0;
            int maxNumber = 0;
            do
            {
                int iNumber = remainOfNumber % 10;

                totalSum += iNumber;
                if (iNumber % 2 != 0)
                {
                    sumForOddNumbers += iNumber;
                }

                if (iNumber > maxNumber)
                {
                    maxNumber = iNumber;
                }

                remainOfNumber = (remainOfNumber - iNumber) / 10;
            }
            while (remainOfNumber > 0);

            Console.WriteLine("сумма всех чисел в числе {0}", totalSum);
            Console.WriteLine("сумма всех нечетных чисел в числе {0}", sumForOddNumbers);
            Console.WriteLine("максимальное число {0}", maxNumber);
        }
    }
}
