﻿using System;

namespace Lesson5
{
	/// <summary>
	/// Реализация алгоритма Евклида.
	/// </summary>
    class EuclideanAlgorithm
    {
        public static void Main()
        {
            int number1 = 320;
            int number2 = 120;
            Console.WriteLine("наибольший общий делитель числа {0} и {1} - {2}", number1, number2, GetGreatestDivisor(number1, number2));
        }

        private static int GetGreatestDivisor(int a, int b)
        {
            int c = 0;
            while (b != 0)
            {
                c = a % b;
                a = b;
                b = c;
            }
            return a;
        }
    }
}
