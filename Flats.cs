﻿using System;

namespace Lesson2
{
	/// <summary>
	/// Поиск по номеру квартиры: подьезда, этажа и положения на этаже.
	/// </summary>
    class Flats
    {
        static void Main()
        {
            Console.WriteLine("Введите кол-во этажей в доме:");
            if (!int.TryParse(Console.ReadLine(), out int floorsCount))
            {
                Console.WriteLine("это не число");
                return;
            }

            Console.WriteLine("Введите кол-во подъездов в доме:");
            if (!int.TryParse(Console.ReadLine(), out int entrancesCount))
            {
                Console.WriteLine("это не число");
                return;
            }

            Console.WriteLine("какую квартиру нужно проверить:");
            if (!int.TryParse(Console.ReadLine(), out int flatNumber))
            {
                Console.WriteLine("это не число");
                return;
            }

            const int flatsOnFloor = 4;
            int flatsInEntrance = floorsCount * flatsOnFloor;

            if (flatNumber <= flatsInEntrance * entrancesCount)
            {
                int flatEntrance = (int)Math.Ceiling((double)flatNumber / flatsInEntrance);
                int flatsInPreviousEntrance = flatsInEntrance * (flatEntrance - 1);
                int flatFloor = (int)Math.Ceiling((double)(flatNumber - flatsInPreviousEntrance) / flatsOnFloor);

                Console.WriteLine("подьезд этой квартиры: {0}", flatEntrance);
                Console.WriteLine("этаж этой квартиры: {0}", flatFloor);

                int flatPosition = flatNumber % flatsOnFloor;
                switch (flatPosition)
                {
                    case 0:
                        Console.WriteLine("положение: ближняя справа");
                        break;
                    case 1:
                        Console.WriteLine("положение: дальняя справа");
                        break;
                    case 2:
                        Console.WriteLine("положение: дальняя слева");
                        break;
                    case 3:
                        Console.WriteLine("положение: ближняя слева");
                        break;
                    default:
                        Console.WriteLine("положение квартиры не определено");
                        break;
                }
            }
            else
            {
                Console.WriteLine("такой квартиры нет в этом доме");
            }
        }
    }
}
