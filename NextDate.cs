﻿using System;
using System.Linq;

namespace Lesson3
{
	/// <summary>
	/// Пользователь вводит в консоль дату и получает дату следующего дня.
	/// </summary>
    class NextDate
    {
        static MyDateTime userDate = new MyDateTime();

        static void Main()
        {
            if (InputValidation())
            {
                userDate = userDate.AddDay();
                Console.WriteLine("дата на следующий день: {0}.{1}.{2}", userDate.Day, userDate.Month.ToString("D2"), userDate.Year);
            }
        }

        static bool InputValidation()
        {
            Console.WriteLine("введите день");
            if (!int.TryParse(Console.ReadLine(), out int userDay))
            {
                Console.WriteLine("это не число!");
                return false;
            }

            Console.WriteLine("введите месяц");
            if (!int.TryParse(Console.ReadLine(), out int userMonth))
            {
                Console.WriteLine("это не число!");
                return false;
            }

            Console.WriteLine("введите год");
            if (!int.TryParse(Console.ReadLine(), out int userYear))
            {
                Console.WriteLine("это не число!");
                return false;
            }

            try
            {
                userDate = new MyDateTime(userDay, userMonth, userYear);
                return true;
            }
            catch (FormatException)
            {
                Console.WriteLine("неверный формат ввода");
                return false;
            }
        }
    }

    public struct MyDateTime
    {
        public int Day { get; private set; }
        public int Month { get; private set; }
        public int Year { get; private set; }

        /// <summary>
        /// создается дата из представленных значений
        /// </summary>
        public MyDateTime(int day, int month, int year)
        {
            this.Year = year;
            this.Month = month;
            this.Day = day;

            if (day > GetDaysInMonth(month, year) || day < 1)
            {
                throw new FormatException();
            }
            if (month > 12 || month < 1)
            {
                throw new FormatException();
            }
            if (year < 1)
            {
                throw new FormatException();
            }
        }

        /// <summary>
        /// Добавляет один день к дате
        /// </summary>
        /// <returns>возвращает новую дату</returns>
        public MyDateTime AddDay()
        {
            if (Day < GetDaysInMonth(Month, Year))
            {
                Day++;
            }
            else
            {
                Day = 1;
                if (Month < 12)
                {
                    Month++;
                }
                else
                {
                    Month = 1;
                    Year++;
                }
            }
            return new MyDateTime(Day, Month, Year);
        }

        bool IsLeapYear(int year)
        {
            return (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0));
        }

        int GetDaysInMonth(int month, int year)
        {
            if (month == 4 || month == 6 || month == 9 || month == 11)
            {
                return 30;
            }
            else if (month == 2)
            {
                if (IsLeapYear(year))
                {
                    return 29;
                }
                else
                {
                    return 28;
                }
            }
            else
            {
                return 31;
            }
        }
    }
}
