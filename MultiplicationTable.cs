﻿using System;

namespace Lesson5
{
	/// <summary>
	/// Создание в консоли таблицы умножения.
	/// </summary>
    class MultiplicationTable
    {
        public static void Main()
        {
            CreateMultiplicationTable(20);
        }

        private static void CreateMultiplicationTable(int size)
        {

            for (int i = 0; i < size; i++)
            {
                if (i > 0)
                {
                    Console.Write("{0,4}|", i);
                }

                for (int j = 0; j < size; j++)
                {
                    if (i == 0)
                    {
                        if (j == 0)
                        {
                            Console.Write("{0,5}", "");
                        }
                        else
                        {
                            Console.Write("__{0,3}", j);
                        }
                    }
                    else
                    {
                        if (j != 0)
                        {
                            Console.Write("{0,5}", i * j);
                        }
                    }
                }

                Console.WriteLine();
            }
        }
    }
}
