﻿using System;

namespace Lesson10
{
	/// <summary>
	/// Реализация бинарного и рекурсивного поиска.
	/// </summary>
    class BinarySearch
    {
        static void Main()
        {
            int[] array = { 0, 1, 4, 34, 44, 66 };
            Console.WriteLine("индекс этого числа {0}", GetIndexRecursia(array, 0, array.Length - 1, 4));
            Console.WriteLine("индекс этого числа {0}", GetIndexBinarySearch(array, 0, array.Length - 1, 4));
        }

        public static int GetIndexBinarySearch(int[] a, int left, int right, int x)
        {
            while (true)
            {
                if (left > right)
                {
                    return -1;
                }

                int middle = (right + left) / 2;

                if (a[middle] < x)
                {
                    left = middle + 1;
                }
                else if (a[middle] > x)
                {
                    right = middle - 1;
                }
                else
                {
                    return middle;
                }
            }
        }

        public static int GetIndexRecursia(int[] a, int left, int right, int x)
        {
            if (left > right)
            {
                return -1;
            }

            int middle = (right + left) / 2;

            if (a[middle] < x)
            {
                return GetIndexRecursia(a, middle + 1, right, x);
            }
            else if (a[middle] > x)
            {
                return GetIndexRecursia(a, left, middle - 1, x);
            }
            else
            {
                return middle;
            }
        }
    }
}
