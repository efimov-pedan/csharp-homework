﻿using System;

namespace Lesson9
{
	/// <summary>
	/// Класс для проверки сортировки
	/// </summary>
    class CheckSort
    {
        public static void Main()
        {
            int[] increaseNumbers = { 1, 2, 2, 4, 5, 6 };
            if (IsIncreaseArray(increaseNumbers))
            {
                Console.WriteLine("массив отсортирован по возрастанию");
            }
            else
            {
                Console.WriteLine("массив не отсортирован по возрастанию");
            }

            int[] decreaseNumbers = { 6, 5, 3, 3, 3, 1 };
            if (IsDecreaseArray(decreaseNumbers))
            {
                Console.WriteLine("массив отсортирован по убыванию");
            }
            else
            {
                Console.WriteLine("массив не отсортирован по убыванию");
            }
        }

        private static bool IsIncreaseArray(int[] a)
        {
            for (int i = 1; i < a.Length; i++)
            {
                if (a[i] < a[i - 1])
                {
                    return false;
                }
            }
            return true;
        }

        private static bool IsDecreaseArray(int[] a)
        {
            for (int i = 1; i < a.Length; i++)
            {
                if (a[i] > a[i - 1])
                {
                    return false;
                }
            }
            return true;
        }
    }
}
