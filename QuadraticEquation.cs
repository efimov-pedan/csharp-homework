﻿using System;

namespace Lesson3
{
	/// <summary>
	/// Класс для решения квадратных уровнений.
	/// </summary>
    class QuadraticEquation
    {
        static void Main()
        {
            double aNumber = SetNumber("a");
            double bNumber = SetNumber("b");
            double cNumber = SetNumber("c");

            double discriminant = (double)Math.Pow(bNumber, 2) - (4 * aNumber * cNumber);
            if (discriminant < 0)
            {
                Console.WriteLine("корней нет");
            }
            else if (discriminant == 0)
            {
                double x = -bNumber / (2 * aNumber);
                Console.WriteLine("один корень {0}", x);
            }
            else
            {
                double x1 = (-bNumber + Math.Sqrt(discriminant)) / (2 * aNumber);
                Console.WriteLine("первый корень {0}", x1);
                double x2 = (-bNumber - Math.Sqrt(discriminant)) / (2 * aNumber);
                Console.WriteLine("второй корень {0}", x2);
            }

        }
        static double SetNumber(string numberName)
        {
            Console.WriteLine("введите коэффициент {0}", numberName);
            if (!double.TryParse(Console.ReadLine(), out double number))
            {
                Console.WriteLine("это не число!");
                return 1;
            }
            if (number == 0)
            {
                number = 1;
            }
            return number;
        }
    }
}
