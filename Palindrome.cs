﻿using System;

namespace Lesson8
{
	/// <summary>
	/// Является ли строка палиндром?
	/// </summary>
    class Palindrome
    {
        public static void Main()
        {
            string myString = "я иду с мечем судия     ";

            if (IsPalindrome(myString))
            {
                Console.WriteLine("это строка палиндром");
            }
            else
            {
                Console.WriteLine("эта строка не палиндром");
            }
        }

        private static bool IsPalindrome(string s)
        {
            int leftIndex = 0;
            int rightIndex = s.Length - 1;

            do
            {
                if (!char.IsLetter(s[leftIndex]))
                {
                    leftIndex++;
                    continue;
                }

                if (!char.IsLetter(s[rightIndex]))
                {
                    rightIndex--;
                    continue;
                }

                if (s[leftIndex] != s[rightIndex])
                {
                    return false;
                }

                leftIndex++;
                rightIndex--;
            } while (leftIndex >= s.Length && rightIndex >= 0);

            return true;
        }
    }
}
