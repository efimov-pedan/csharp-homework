﻿using System;

namespace Lesson11
{
	/// <summary>
	/// Реализация пузырьковой сортировки.
	/// </summary>
    class BubbleSort
    {
        public static void Main()
        {
            int[] numbers = { -3, 2, 1, -5, 11, 14, 41 };

            SortArray(numbers);
            PrintArray(numbers);
        }

        private static void SortArray(int[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                bool isSorted = true;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] > array[j])
                    {
                        int temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                        isSorted = false;
                    }
                }
                if (isSorted)
                {
                    return;
                }
            }
        }

        private static void PrintArray(int[] array)
        {
            foreach (int item in array)
            {
                Console.Write("{0},", item);
            }
        }
    }
}
