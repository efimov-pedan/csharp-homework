﻿using System;

namespace Lesson9
{
	/// <summary>
	/// Решение задач на тему "Массивы".
	/// </summary>
    class ArrayTasks
    {
        static void Main()
        {
            double[] arrayNumbers1 = { -1, -5, -55, -114, -5243453, -22, -112 };
            Console.WriteLine("максимальное число в массиве {0}", GetMaxNumberInArray(arrayNumbers1));

            int[] arrayNumbers2 = { 1, 14, 55, 114, 5243453, 22, 112 };
            Console.WriteLine("позиция этого числа в массиве {0}", GetNumberPositionInArray(arrayNumbers2, 2));

            string[] words = { "asd", "asdasd", "UIHHIIJHS" };
            SetStringArrayToUpper(words);

            int[] numbers = { 1, 2, 3, 4 };

            Console.WriteLine("среднее арифметическое этого массива {0}", GetAverage(numbers));

            PrintArray(numbers);
            ReverseArray(numbers);
            PrintArray(numbers);
        }

		/// <summary>
		/// Возвращает среднее арифметическое массива.
		/// </summary>
        private static double GetAverage(int[] array)
        {
            int sum = 0;
            int evenNumbersCounts = 0;

            foreach (int item in array)
            {
                if (item % 2 == 0)
                {
                    sum += item;
                    evenNumbersCounts++;
                }
            }
            return (double)sum / evenNumbersCounts;
        }

		/// <summary>
		/// Распечатка массива.
		/// </summary>
        private static void PrintArray(int[] array)
        {
            foreach (int item in array)
            {
                Console.Write("{0},", item);
            }
            Console.WriteLine();
        }

		/// <summary>
		/// Разворот массива в обратном порядке.
		/// </summary>
        private static void ReverseArray(int[] userArray)
        {
            int j = 0;
            for (int i = userArray.Length - 1; i != 0; i--)
            {
                int c = userArray[j];
                userArray[j] = userArray[i];
                userArray[i] = c;
                j++;
            }
        }

		/// <summary>
		/// Преобразование элементов в элементы с верхним регистром.
		/// </summary>
        private static void SetStringArrayToUpper(string[] userArray)
        {
            for (int i = 0; i < userArray.Length; i++)
            {
                userArray[i] = userArray[i].ToUpper();
            }
        }

		/// <summary>
		/// Получение позиции элемента в массиве
		/// </summary>
        private static int GetNumberPositionInArray(int[] a, int userNumber)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (userNumber == a[i])
                {
                    return i;
                }
            }
            return -1;
        }

		/// <summary>
		/// Получение максимального числа в массиве
		/// </summary>
        private static double GetMaxNumberInArray(double[] a)
        {
            double maxNumber = a[0];
            foreach (double number in a)
            {
                if (maxNumber < number)
                {
                    maxNumber = number;
                }
            }
            return maxNumber;
        }
    }
}
