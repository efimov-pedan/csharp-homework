﻿using System;

namespace Lesson12
{
	/// <summary>
	/// Вычленяет из URL адреса имя сервера.
	/// </summary>
    class UrlTask
    {
        public static void Main()
        {
            Console.WriteLine("имя сервера: {0}", GetServerName("https://deezer/343"));
        }

        private static string GetServerName(string url)
        {
            const string urlFormat = "://";
            const string slash = "/";

            int startIndex = url.IndexOf(urlFormat, StringComparison.Ordinal) + urlFormat.Length;
            int endIndex = url.IndexOf(slash, startIndex, StringComparison.Ordinal);

            if (endIndex == -1)
            {
                return url.Substring(startIndex);
            }
            else
            {
                return url.Substring(startIndex, endIndex - startIndex);
            }
        }
    }
}
