﻿using System;

namespace Lesson11
{
	/// <summary>
	/// Реализация сортировки выбором.
	/// </summary>
    class SortByChoise
    {
        public static void Main()
        {
            int[] numbers = { 3, 0, 2, 5, -1, 4, 1 };

            SortArray(numbers);
            PrintArray(numbers);
        }

        private static void SortArray(int[] array)
        {
            for (int i = 0; i < array.Length - 1; i++)
            {
                int min = i;
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[j] < array[min])
                    {
                        min = j;
                    }
                }
                int temp = array[min];
                array[min] = array[i];
                array[i] = temp;
            }
        }

        private static void PrintArray(int[] array)
        {
            foreach (int item in array)
            {
                Console.Write("{0},", item);
            }
        }
    }
}
