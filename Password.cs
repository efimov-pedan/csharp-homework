﻿using System;

namespace Lesson3
{
	/// <summary>
	/// Проверка пароля.
	/// </summary>
    class Password
    {
        static void Main()
        {
            const string password = "321";
            Console.WriteLine("Введите пароль:");
            string userPassword = "";
            ConsoleKeyInfo inputKey = new ConsoleKeyInfo();

            while (inputKey.Key != ConsoleKey.Enter)
            {
                inputKey = Console.ReadKey(true);
                if (inputKey.Key == ConsoleKey.Backspace)
                {
                    if (userPassword.Length != 0)
                    {
                        userPassword = userPassword.Remove(userPassword.Length - 1);
                        Console.Write("\b \b");
                    }
                }
                else
                {
                    if (inputKey.Key != ConsoleKey.Enter)
                    {
                        userPassword += inputKey.KeyChar;
                        Console.Write("*");
                    }
                }
            }

            Console.WriteLine("");
            if (userPassword.Equals(password))
            {
                Console.WriteLine("Пароль верный");
            }
            else
            {
                if (userPassword.Length > password.Length)
                {
                    Console.WriteLine("пароль неверный, строка слишком длинная");
                }
                else if (userPassword.Length < password.Length)
                {
                    Console.WriteLine("пароль неверный, строка слишком короткая");
                }
                else
                {
                    Console.WriteLine("пароль неверный");
                }
            }
        }
    }
}
